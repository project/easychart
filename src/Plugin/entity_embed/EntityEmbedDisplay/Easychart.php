<?php

namespace Drupal\easychart\Plugin\entity_embed\EntityEmbedDisplay;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\easychart\EasychartHelper;
use Drupal\entity_embed\EntityEmbedDisplay\EntityEmbedDisplayBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity Embed Display to render Easycharts.
 *
 * @see \Drupal\entity_embed\EntityEmbedDisplay\EntityEmbedDisplayInterface
 *
 * @EntityEmbedDisplay(
 *   id = "easychart",
 *   label = @Translation("Easychart"),
 * )
 */
class Easychart extends EntityEmbedDisplayBase {

  /**
   * Module extension helper.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->moduleExtensionList = $container->get('extension.list.module');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $output = [];
    $route_match = \Drupal::routeMatch();

    $entity = $this->getEntityFromContext();
    $values = $entity->get('easychart')->getValue();

    if ($entity instanceof ContentEntityBase) {
      $translation = $entity->getTranslation($this->getLangcode());
      $values = $translation->get('easychart')->getValue();
    }

    // We are in preview.
    if ($route_match->getRouteName() == 'embed.preview') {
      $show_placeholder = TRUE;

      // Try to render preview in the editor.
      if (!empty($values[0]['config'])) {
        $http_client = \Drupal::httpClient();
        $options = [];
        $options['options'] = $values[0]['config'];
        $options['type'] = 'image/png';
        $options['async'] = TRUE;
        $response = $http_client->post('http://export.highcharts.com', ['form_params' => (object) $options]);
        if ($response->getStatusCode() == 200) {
          $body = $response->getBody();
          $content = $body->getContents();
          if (!empty($content)) {
            $show_placeholder = FALSE;
            $output = [
              '#markup' => '<img src="http://export.highcharts.com/' . $content . '">',
            ];
          }
        }
      }

      // Show placeholder.
      if ($show_placeholder) {
        $output = [
          '#markup' => '<img src="' . base_path() . $this->moduleExtensionList->getPath('easychart') . '/assets/chart-placeholder.png">',
        ];
      }
    }
    // Otherwise, render the chart.
    else {
      $delta = 0;
      $entity_id = $entity->id();
      $output = EasychartHelper::easychartPrintChart($values[0], $entity_id, $delta);
      if (!empty($output)) {
        $element[$delta] = ['#markup' => $output['markup']];
        $element['#attached']['drupalSettings']['easychart'][$output['identifier']] = [
          'config' => $output['config'],
          'csv' => $output['csv'],
        ];
      }

      if (!empty($element)) {
        EasychartHelper::addRenderLibraries($element);
        $output = $element;
      }
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [];
  }

}
