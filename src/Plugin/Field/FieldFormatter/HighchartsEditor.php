<?php

namespace Drupal\easychart\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\easychart\EasychartHelper;

/**
 * Provides a default Easychart formatter.
 *
 * @FieldFormatter(
 *   id = "highcharts_editor",
 *   module = "easychart",
 *   label = @Translation("HighEd"),
 *   field_types = {
 *     "easychart"
 *   },
 *   quickedit = {
 *     "editor" = "disabled"
 *   }
 * )
 */
class HighchartsEditor extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Unique distinction by $entity_id & $delta.
    $entity_id = $items->getEntity()->id();
    $field_name = $this->fieldDefinition->getFieldStorageDefinition()->getName();
    $element = [];

    foreach ($items as $delta => $item) {
      $output = EasychartHelper::highcartsPrintChart($item->getValue(), $entity_id, $delta, $field_name);
      if (!empty($output)) {
        $element[$delta] = ['#markup' => $output['markup']];
        $element['#attached']['drupalSettings']['highcharts'][$output['identifier']] = [
          'config' => $output['config'],
        ];
      }
    }

    if (!empty($element)) {
      EasychartHelper::addRenderLibraries($element, FALSE);
    }

    return $element;
  }

}
