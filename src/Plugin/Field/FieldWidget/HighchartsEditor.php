<?php

namespace Drupal\easychart\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a default Highcharts Editor widget.
 *
 * @FieldWidget(
 *   id = "highcharts_editor",
 *   label = @Translation("Highcharts Editor"),
 *   field_types = {
 *     "easychart"
 *   }
 * )
 */
class HighchartsEditor extends WidgetBase {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $configuration;

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configuration = $container->get('config.factory')->get('easychart.settings');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $items->getFieldDefinition()->getName();
    $entity = $items->getEntity();
    $values = $entity->get($field_name)->getValue();
    $identifier = $field_name . '-' . $delta;
    $element['#attached']['library'][] = 'easychart/lib.highcharts';
    $element['#attached']['library'][] = 'easychart/lib.highcharts-editor';
    $element['#attached']['library'][] = 'easychart/highchartseditor.widget';

    // Provide a modal window for the Highcharts Editor.
    $element['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['highcharts-editor-wrapper', 'highed-box-size'],
      ],
    ];

    $element['container']['config'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['highed-box-size', 'highed-config'],
        'id' => $identifier . '-config',
      ],
      '#default_value' => $values[$delta]['config'] ?? NULL,
    ];

    $element['container']['csv'] = [
      '#type' => 'hidden',
      '#default_value' => $values[$delta]['csv'] ?? NULL,
      '#attributes' => [
        'id' => $identifier . '-csv',
      ],
    ];

    $element['container']['csv_url'] = [
      '#type' => 'hidden',
      '#default_value' => $values[$delta]['csv_url'] ?? NULL,
    ];

    $modal_button = [
      '#type' => 'button',
      '#is_button' => TRUE,
      '#attributes' => [
        'class' => ['highed-imp-button'],
        'config-target' => $identifier . '-config',
        'csv-target' => $identifier . '-csv',
        'id' => 'modal-btn-' . $delta,
        'type' => 'button',
      ],
      '#pre_render' => [],
      '#value' => $values[$delta]['config'] ? $this->t('Edit chart') : $this->t('Create chart'),
    ];
    Element::setAttributes($modal_button, ['id', 'name', 'value']);

    $element['container']['modal-button'] = $modal_button;

    return $element;
  }

  /**
   * CSV validate method.
   *
   * @param array $element
   *   The csv element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface.
   */
  public static function validateCsvElement(array $element, FormStateInterface $form_state) {
    if ($element['#csv_required'] && empty($element['#value'])) {
      $form_state->setError($element, t('Please create an Easychart chart before saving.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    // The widget form element returns an associative array with hidden
    // form elements, so we need to re-assign those values at the right $values
    // array keys.
    $i = 0;
    foreach ($values as &$value) {
      $value = $values[$i]['container'];
      $i++;
    }

    return $values;
  }

}
