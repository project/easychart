<?php

namespace Drupal\easychart\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a default Easychart widget.
 *
 * @FieldWidget(
 *   id = "easychart_default",
 *   label = @Translation("Chart"),
 *   field_types = {
 *     "easychart"
 *   }
 * )
 */
class Easychart extends WidgetBase {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $configuration;

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configuration = $container->get('config.factory')->get('easychart.settings');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    // Get easychart entity field values.
    $field_name = $items->getFieldDefinition()->getName();
    $entity = $items->getEntity();
    $values = $entity->get($field_name)->getValue();

    // Get the options.
    $options = '';
    if (file_exists('public://easychart-options.json')) {
      $options = file_get_contents('public://easychart-options.json');
    }

    $settings = [
      'easychartOptions' => $options,
      'easychartPresets' => $this->configuration->get('presets'),
      'easychartTemplates' => $this->configuration->get('templates'),
      'easychartCustomize' => $this->currentUser->hasPermission('access full easychart configuration'),
    ];

    // Attach settings and libraries to render array.
    $element['#attached']['drupalSettings']['easychart'] = $settings;
    $element['#attached']['library'][] = 'easychart/easychart.widget';
    $element['#attached']['library'][] = 'easychart/lib.highcharts';
    $element['#attached']['library'][] = 'easychart/lib.easycharts.full';

    $element['container'] = [
      '#prefix' => '<div class="easychart-wrapper clearfix entity-meta">',
      '#suffix' => '</div>',
      '#type' => 'container',
      '#attributes' => [
        'class' => ['entity-meta__header clearfix'],
        'style' => ['padding:0;'],
      ],
    ];

    $element['container']['config'] = [
      '#description' => $this->t('The configuration options as described at http://api.highcharts.com/highcharts'),
      '#type' => 'hidden',
      '#default_value' => $values[$delta]['config'] ?? NULL,
      '#attributes' => ['class' => ['easychart-config']],
    ];

    $element['container']['csv'] = [
      '#type' => 'hidden',
      '#description' => $this->t('Your chart data in CSV format'),
      '#default_value' => $values[$delta]['csv'] ?? NULL,
      '#attributes' => ['class' => ['easychart-csv']],
      '#element_validate' => [[get_called_class(), 'validateCsvElement']],
      '#csv_required' => $element['#required'],
    ];

    $element['container']['csv_url'] = [
      '#type' => 'hidden',
      '#description' => $this->t('The URL to a CSV file'),
      '#default_value' => $values[$delta]['csv_url'] ?? NULL,
      '#attributes' => ['class' => ['easychart-csv-url']],
    ];

    $element['container']['preview'] = [
      '#title' => $this->t('Easychart'),
      '#markup' => '',
      '#prefix' => '<div class="easychart-embed"><div class="easychart-header"><span class="toggle">' . $this->t('Toggle Fullscreen') . '</span></div>',
      '#suffix' => '</div>',
    ];

    return $element;
  }

  /**
   * CSV validate method.
   *
   * @param array $element
   *   The csv element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface.
   */
  public static function validateCsvElement(array $element, FormStateInterface $form_state) {
    if ($element['#csv_required'] && empty($element['#value'])) {
      $form_state->setError($element, t('Please create an Easychart chart before saving.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    // The EasychartWidget form element returns an associative array with hidden
    // form elements, so we need to re-assign those values at the right $values
    // array keys.
    $i = 0;
    foreach ($values as &$value) {
      $value = $values[$i]['container'];
      $i++;
    }

    return $values;
  }

}
