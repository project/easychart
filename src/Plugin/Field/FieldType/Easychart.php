<?php

namespace Drupal\easychart\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the Easychart field type.
 *
 * @FieldType(
 *   id = "easychart",
 *   label = @Translation("Easy chart"),
 *   category = @Translation("Chart"),
 *   default_widget = "easychart_default",
 *   default_formatter = "easychart_default"
 * )
 */
class Easychart extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [];

    $properties['csv'] = DataDefinition::create('string')
      ->setLabel(t('CSV'));
    $properties['csv_url'] = DataDefinition::create('string')
      ->setLabel(t('CSV URL'));
    $properties['config'] = DataDefinition::create('string')
      ->setLabel(t('Config'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'csv' => [
          'description' => 'CSV',
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
        'csv_url' => [
          'description' => 'CSV URL',
          'type' => 'text',
          'size' => 'medium',
          'not null' => FALSE,
        ],
        'config' => [
          'description' => 'Configuration',
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('config')->getValue();
    return $value === NULL || $value === '';
  }

}
