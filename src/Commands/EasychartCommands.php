<?php

namespace Drupal\easychart\Commands;

use Drupal\Core\Archiver\Zip;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class EasychartCommands extends DrushCommands {

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Library discovery.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Constructs new EasychartCommands.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   Library discovery.
   */
  public function __construct(FileSystemInterface $fileSystem, ModuleHandlerInterface $moduleHandler, MessengerInterface $messenger, LibraryDiscoveryInterface $libraryDiscovery) {
    $this->fileSystem = $fileSystem;
    $this->moduleHandler = $moduleHandler;
    $this->messenger = $messenger;
    $this->libraryDiscovery = $libraryDiscovery;
  }

  /**
   * Install dependencies.
   *
   * @usage drush easychart:install
   *   Install JavaScript libraries for Easychart.
   *
   * @command easychart:install
   * @aliases eci
   */
  public function install() {
    // Make sure the Easychart module is enabled.
    if (!$this->moduleHandler->moduleExists('easychart')) {
      $this->messenger->addError(dt('Please enable the Easychart module first.'));
      return;
    }

    // Path variables.
    $libraries_path = 'libraries';

    // Store the old directory.
    $old_dir = getcwd();

    // Create the libraries folder if it does not exist.
    if (!is_dir($libraries_path)) {
      drush_op('mkdir', $libraries_path);
      $this->messenger->addMessage(dt('Directory @path was created', ['@path' => $libraries_path]));
    }

    // Go to libraries path.
    chdir($libraries_path);

    // Install Easychart library.
    $easychart_dir_name = 'easychart';
    $easychart_library = $this->libraryDiscovery->getLibraryByName('easychart', 'lib.easycharts.full');

    $data = file_get_contents($easychart_library['remote']);
    $local = $this->fileSystem->saveData($data, 'easychart.zip', FileSystemInterface::EXISTS_REPLACE);

    if (!empty($easychart_library) && $local) {
      // Remove Easychart library directory.
      if (is_dir($easychart_dir_name)) {
        $this->fileSystem->deleteRecursive($easychart_dir_name);
        $this->messenger->addMessage(dt('An existing Easychart plugin was deleted from @libraries_path.', ['@libraries_path' => $libraries_path]));
      }

      $zipFile = new Zip($local);
      $zipFile->extract('.');
      $this->fileSystem->move('easychart-master', $easychart_dir_name);
      $this->fileSystem->delete($local);
    }

    if (is_dir($easychart_dir_name)) {
      $this->messenger->addMessage(dt('Easychart library has been installed in /@libraries_path.', ['@libraries_path' => $libraries_path]));
    }
    else {
      $this->messenger->addError(dt('Drush was unable to install the Easychart library in /@libraries_path.', ['@libraries_path' => $libraries_path]));
    }

    // Install Highcharts library.
    $highcharts_dir_name = 'highcharts';
    $highcharts_library = $this->libraryDiscovery->getLibraryByName('easychart', 'lib.highcharts');
    $data = file_get_contents($highcharts_library['remote']);
    $local = $this->fileSystem->saveData($data, 'highcharts.zip', FileSystemInterface::EXISTS_REPLACE);
    if (!empty($highcharts_library) && $local) {
      // Remove any existing Highcharts library directory.
      if (is_dir($highcharts_dir_name)) {
        $this->fileSystem->deleteRecursive($highcharts_dir_name);
        $this->messenger->addMessage(dt('An existing Highcharts plugin was deleted from @libraries_path.', ['@libraries_path' => $libraries_path]));
      }

      $zipFile = new Zip($local);
      $zipFile->extract($highcharts_dir_name);
      $this->fileSystem->delete($local);
    }

    if (is_dir($highcharts_dir_name)) {
      $this->messenger->addMessage(dt('Highcharts library has been installed in /@libraries_path.', ['@libraries_path' => $libraries_path]));
    }
    else {
      $this->messenger->addError(dt('Drush was unable to install the Highcharts library in /@libraries_path.', ['@libraries_path' => $libraries_path]));
    }

    // Install Highcharts Editor.
    $highed_dir_name = 'highcharts-editor';
    $highed_library = $this->libraryDiscovery->getLibraryByName('easychart', 'lib.highcharts-editor');

    $first = TRUE;
    foreach ($highed_library['remote'] as $remote) {
      $data = file_get_contents($remote);
      $local = $this->fileSystem->saveData($data, 'highed.zip', FileSystemInterface::EXISTS_REPLACE);
      if (!empty($highed_library) && $local) {
        // Remove any existing Highcharts library directory.
        if (is_dir($highed_dir_name) && $first) {
          $this->fileSystem->deleteRecursive($highed_dir_name);
          $this->messenger->addMessage(dt('An existing Highcharts Editor plugin was deleted from @libraries_path.', ['@libraries_path' => $libraries_path]));
        }
        $first = FALSE;

        $zipFile = new Zip($local);
        $zipFile->extract($highed_dir_name);
        $this->fileSystem->delete($local);
      }
    }

    // Set working directory back to the previous working directory.
    chdir($old_dir);
  }

}
