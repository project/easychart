<?php

namespace Drupal\easychart;

/**
 * Helper class for Easychart.
 */
class EasychartHelper {

  /**
   * Helper function to print the actual chart.
   *
   * @param array $values
   *   The field item values.
   * @param int $entity_id
   *   The entity id.
   * @param int $delta
   *   The delta.
   * @param string $field_name
   *   The field name.
   *
   * @return array
   *   The field output.
   */
  public static function easychartPrintChart(array $values, int $entity_id, int $delta, string $field_name = 'easychart'): array {
    $output = [];

    // Verify csv being given.
    if (empty($values['csv'])) {
      return $output;
    }

    // Add JS for each instance when config is set.
    // Print a div for js to pick up & render chart.
    $output['identifier'] = $entity_id . '-' . $delta . '-' . $field_name;
    $output['markup'] = '<div class="easychart-embed--' . $output['identifier'] . '"></div>';
    // Add config to output.
    $output['config'] = $values['config'];
    // Add csv to output.
    $output['csv'] = (!empty($values['csv'])) ? $values['csv'] : '';
    $output['presets'] = \Drupal::config('easychart.settings')->get('presets');
    return $output;
  }

  /**
   * Add libraries to render a chart.
   *
   * @param array $element
   *   Element array.
   * @param bool $isEasychart
   *   Is it an Easychart generated graph or Highcharts Editor?
   */
  public static function addRenderLibraries(array &$element, $isEasychart = TRUE) {
    if ($isEasychart) {
      $element['#attached']['library'][] = 'easychart/easychart.render';
      $element['#attached']['library'][] = 'easychart/lib.easycharts.render';
    }
    else {
      $element['#attached']['library'][] = 'easychart/highcharts.render';
    }
    $element['#attached']['library'][] = 'easychart/lib.highcharts';
  }

  /**
   * Print chart that has been created with Highcharts Editor.
   *
   * @param array $values
   *   The field item values.
   * @param int $entity_id
   *   The entity id.
   * @param int $delta
   *   The delta.
   * @param string $field_name
   *   The field name.
   *
   * @return array
   *   The field output.
   */
  public static function highcartsPrintChart(array $values, int $entity_id, int $delta, string $field_name = 'easychart'): array {
    $output = [];

    if (empty($values['config'])) {
      return $output;
    }

    $output['identifier'] = $entity_id . '-' . $delta . '-' . $field_name;
    $output['markup'] = '<div class="highcharts-embed--' . $output['identifier'] . '" id="' . $output['identifier'] . '"></div>';
    $output['config'] = $values['config'];
    return $output;
  }

}
