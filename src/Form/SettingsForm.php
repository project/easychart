<?php

namespace Drupal\easychart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'easychart_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'easychart.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL): array {
    $config = $this->config('easychart.settings');

    $options = [3600, 10800, 21600, 43200, 86400, 604800];

    $form['url_update_frequency'] = [
      '#type' => 'select',
      '#options' => [0 => $this->t('Never')] + array_map(
        [$this->dateFormatter, 'formatInterval'],
        array_combine($options, $options)
      ),
      '#title' => $this->t('External data update interval'),
      '#description' => $this->t('Data from external csv files are cached for performance reasons. Decide how often this data should be refreshed.'),
      '#default_value' => $config->get('url_update_frequency'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('easychart.settings')
      ->set('url_update_frequency', $values['url_update_frequency'])
      ->save();
  }

}
