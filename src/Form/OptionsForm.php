<?php

namespace Drupal\easychart\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Options form.
 */
class OptionsForm extends FormBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system service.
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'easychart_admin_options_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL): array {
    $default_options = '';
    if (file_exists('public://easychart-options.json')) {
      $default_options = file_get_contents('public://easychart-options.json');
    }

    $form['options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Available options'),
      '#title_display' => 'invisible',
      '#description' => $this->t('These Highcharts options will be configurable in the Easychart interface when creating/editing a chart. See <a href="@url" target="_blank">http://api.highcharts.com/highcharts</a> for all available options.', ['@url' => Url::fromUri('http://api.highcharts.com/highcharts')->toUriString()]),
      '#default_value' => $default_options,
      '#attributes' => ['class' => ['easychart-options']],
      '#rows' => 15,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#submit' => ['::submitForm'],
      '#button_type' => 'primary',
    ];

    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset to defaults'),
      '#submit' => ['::resetForm'],
      '#limit_validation_errors' => [],
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Merge options with saved options.
    $current_options = json_decode($form_state->getValue('options'));

    // Get all options.
    $full_options = json_decode(file_get_contents(DRUPAL_ROOT . '/libraries/easychart/src/js/config/dump.json'), TRUE);
    $all_options = [];
    foreach ($full_options as $key => $value) {
      $all_options[$value['fullname']] = $value;
    }

    // Start iterating and find the panes.
    if (!empty($current_options)) {
      foreach ($current_options as $key => $info) {
        if (isset($info->panes)) {
          foreach ($info->panes as $s_key => $s_value) {
            if (isset($s_value->options)) {
              foreach ($s_value->options as $ss_key => $ss_value) {
                if ($ss_value->fullname) {
                  $current_options[$key]->panes[$s_key]->options[$ss_key] = (object) array_merge((array) $ss_value, $all_options[$ss_value->fullname]);
                }
              }
            }
          }
        }
      }

      // Write to file.
      $this->fileSystem->saveData(json_encode($current_options, JSON_PRETTY_PRINT), 'public://easychart-options.json', FileSystemInterface::EXISTS_REPLACE);
      $this->messenger()->addMessage($this->t('The configuration options have been saved.'));
    }
    else {
      $this->messenger()->addMessage($this->t('Something went wrong saving the options.'));
    }
  }

  /**
   * Redirect to reset form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('easychart.reset_options_confirm_form');
  }

}
