(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.easychartRender = {
    attach: function (context, settings) {
      if (settings.highcharts != undefined) {
        var charts = settings.highcharts;
        $.each(charts, function (key, el) {
          var $container = $('.highcharts-embed--' + key)[0];
          Highcharts.chart($container, JSON.parse(charts[key].config));
        });
      }
    }
  }
})(jQuery, Drupal);
