(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.highchartsEditorWidget = {
    attach: function (context, settings) {
      highed.ready(function () {
        const buttons = once('highed-modal-button', '.highed-imp-button', context);

        $(buttons).each(function(i, el) {
          const currentOptions = getConfig(el);
          let csv = getCsv(el);

          if (currentOptions.data !== undefined && csv.length === 0) {
            csv = currentOptions.data.csv;
          }

          const modal = highed.ModalEditor(el, {
            defaultChartOptions: currentOptions,
            allowDone: true,
            features: 'import templates customize done',
            importer: {
              options: 'plugins csv json'
            }
          }, function (chart) {
            const json = chart.export.json(true);
            const csv = modal.editor.dataTable.toData();
            $('#' + $(el).attr('config-target')).val(JSON.stringify(json));
            $('#' + $(el).attr('csv-target')).val(JSON.stringify(csv));
          });

          highed.dom.on(highed.dom.get(el), 'click', function() {
            csv = getCsv(el);
            modal.editor.dataTable.clear();
            modal.editor.dataTable.loadCSV({csv: csv});
          });
        });
      });
    }
  }

  function getConfig(el) {
    const configTarget = $(el).attr('config-target');
    let currentOptions = {};

    if (configTarget !== undefined) {
      const configValue = $('#' + configTarget).val();
      if (configValue.length > 0) {
        currentOptions = JSON.parse(configValue);
      }
    }

    return currentOptions;
  }

  function getCsv(el) {
    const csvTarget = $(el).attr('csv-target');
    let csv = '';

    if (csvTarget !== undefined) {
      let csvValue = $('#' + csvTarget).val();

      if (csvValue != 'false' && csvValue !== false && csvValue.length > 0) {
        const jsonCsv = JSON.parse(csvValue);

        if (jsonCsv != 'false' && jsonCsv !== false) {
          csv = jsonCsv.map(function (d) {
            return d.join(';');
          }).join("\n");
        }
      }
    }

    return csv;
  }
})(jQuery, Drupal);

